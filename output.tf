output "bastion_public_ip" {
  value = module.bastion_server.bastion_ip
}

output "jenkins-servers-ip" {
  value = module.jenkins_server.jenkins_ip
}

output "ansible-servers-ip" {
  value = module.ansible_server.ansible_ip
}
output "sonar-ip" {
  value = "http://${module.sonar.sonar_pub_ip}:9000"
  #value = module.sonar.sonar_pub_ip
}

output "nexus-ip" {
  value = join("", ["http://", module.nexus_server.nexus-pub-ip, ":", "8081"])
  #value = module.nexus_server.nexus-pub-ip
}

output "prod_dns_name" {
  value = "https://${var.prod_record_name}"
}

output "stage_dns_name" {
  value = "https://${var.stage_record_name}"
}

output "jenkins_dns_name" {
  value = "https://${var.jenkins_record_name}"
}

