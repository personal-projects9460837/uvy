module "vpc" {
    source = "./modules/vpc"
    vpc-cidr         = var.vpc-cidr
    subnet-cidr-pub  = var.subnet-cidr-pub
    subnet-cidr-priv = var.subnet-cidr-priv
    pub-subnet       = var.pub-subnet
    priv-subnet      = var.priv-subnet
    route-cidr       = var.route-cidr

}

module "security_gp" {
  source  = "./modules/security-group"
  sg-cidr = var.sg-cidr
  vpc_id  = module.vpc.vpc-id
  ansible-priv-ip = module.ansible_server.ansible_ip
  jenkins-priv-ip = module.jenkins_server.jenkins_ip
}

module "key_pair" {
  source = "./modules/keypair"
}

module "ansible_server" {
  source           = "./modules/ansible"
  ami_ubuntu       = module.key_pair.ami_ubuntu
  key_pair         = module.key_pair.pub_key
  priv_subnet1     = module.vpc.priv-sub1
  ansible_sg       = module.security_gp.ansible_sg
  priv_key         = module.key_pair.priv_key
  instance_profile = module.role.instance_profile
  bastion-host     = module.bastion_server.bastion_ip
  nexus_pub_ip     = module.nexus_server.nexus-pub-ip
  pub_key          = module.key_pair.pub_key

}

module "bastion_server" {
  source = "./modules/bastion"
  ami_ubuntu    = module.key_pair.ami_ubuntu
  key_pair      = module.key_pair.pub_key
  priv_key      = module.key_pair.priv_key
  pub_subnet1   = module.vpc.pub-sub-1
  bastion_sg    = module.security_gp.bastion_sg

}


module "jenkins_server" {
  source = "./modules/jenkins"
  ami_ubuntu = module.key_pair.ami_ubuntu
  key_pair = module.key_pair.pub_key
  priv_subnet1 = module.vpc.priv-sub1
  jenkins_sg = module.security_gp.jenkins_sg
  nexus_pub_ip = module.nexus_server.nexus-pub-ip
}

module "sonar" {
  source = "./modules/sonarqube"
  ami_ubuntu = module.key_pair.ami_ubuntu
  key_pair = module.key_pair.pub_key
  pub-sub-1 = module.vpc.pub-sub-1
  sonar_sg = module.security_gp.sonar_sg
}

module "nexus_server" {
  source      = "./modules/nexus"
  ami_ubuntu  = module.key_pair.ami_ubuntu
  key_pair    = module.key_pair.pub_key
  pub_subnet1 = module.vpc.pub-sub-1
  nexus_sg    = module.security_gp.nexus_sg
}

module "secret-kms" {
  source      = "./modules/secret-manager-kms"
}

data "aws_secretsmanager_secret" "rds_secret" {
  name = "rds_admin"
  depends_on = [ aws_secretsmanager_secret.rds ]
}
data "aws_secretsmanager_secret_version" "secret_version" {
  secret_id = data.aws_secretsmanager_secret.rds_secret.id
}

module "rds" {
  source = "./modules/rds"
  priv_subnet1 = module.vpc.priv-sub1
  priv_subnet2 = module.vpc.priv-sub2
  rds_sg       = module.security_gp.rds_sg
  dbName       = var.dbName 
  dbPassword   = data.aws_secretsmanager_secret_version.secret_version.secret_string
  dbUsername   = var.dbUsername
}

module "role" {
  source = "./modules/roles"
}

module "route53" {
  source               = "./modules/route53"
  domain_name          = var.domain_name
  domain_name2         = var.domain_name2
  jenkins_record_name  = var.jenkins_record_name
  prod_record_name     = var.prod_record_name
  stage_record_name    = var.stage_record_name
  jenkins_lb_dns_name  = module.jenkins_server_lb.jenkins_dns_name
  prod_lb_dns_name     = module.prod_lb.prod_dns_name
  stage-lb-dns-name    = module.stage_lb.stage_dns_name
  jenkins_lb_zone_id   = module.jenkins_server_lb.jenkins_zone_id
  prod_lb_zone_id      = module.prod_lb.prod_zone_id
  stage-lb-zone-id     = module.stage_lb.stage_zone_id

}

module "jenkins_server_lb" {
  source = "./modules/jenkins-lb"
  lb_sg = module.security_gp.lb_sg
  pub_subnet_1 = module.vpc.pub-sub-1
  pub_subnet_2 = module.vpc.pub-sub-2
  vpc_id = module.vpc.vpc-id
  jenkins_server_id = module.jenkins_server.jenkins_id
  certificate_arn = module.route53.certificate_arn

}

module "prod_lb" {
  source = "./modules/prod-lb"
  pub_subnet_1 = module.vpc.pub-sub-1
  pub_subnet_2 = module.vpc.pub-sub-2
  vpc_id = module.vpc.vpc-id
  lb_sg   = module.security_gp.lb_sg
  certificate_arn = module.route53.certificate_arn

}

module "stage_lb" {
  source = "./modules/stage-lb"
  lb_sg   = module.security_gp.lb_sg
  pub_subnet_1 = module.vpc.pub-sub-1
  pub_subnet_2 = module.vpc.pub-sub-2
  vpc_id = module.vpc.vpc-id
  certificate_arn = module.route53.certificate_arn

}

module "prod_dock_asg" {
  source = "./modules/prod-asg"
  priv_subnet1 = module.vpc.priv-sub1
  priv_subnet2 = module.vpc.priv-sub2
  prod_target_gp_arn = module.prod_lb.prod_target_gp_arn
  instance_type = var.instance_type
  ami_ubuntu = module.key_pair.ami_ubuntu
  key_pair = module.key_pair.pub_key
  docker_prod_sg = module.security_gp.docker_prod_sg

}

module "stage_dock_asg" {
  source = "./modules/stage-asg"
  priv_subnet1 = module.vpc.priv-sub1
  priv_subnet2 = module.vpc.priv-sub2
  stage_target_gp_arn = module.stage_lb.stage_target_gp_arn
  instance_type = var.instance_type
  ami_ubuntu = module.key_pair.ami_ubuntu
  key_pair = module.key_pair.pub_key
  docker_stage_sg = module.security_gp.docker_stage_sg

}