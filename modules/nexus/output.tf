output "nexus-pub-ip" {
  value = aws_instance.nexus_instance.public_ip
}