# launch the nexus instance
resource "aws_instance" "nexus_instance" {
  ami                    = var.ami_ubuntu
  instance_type          = "t2.medium"
  subnet_id              = var.pub_subnet1
  vpc_security_group_ids = [var.nexus_sg]
  key_name               = var.key_pair
  user_data              = local.nexus_user_data

  tags = {
    Name = "nexus-server"
  }
}