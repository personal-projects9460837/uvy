locals {
  nexus_user_data = <<-EOF
#!/bin/bash
sudo hostnamectl set-hostname nexus
sudo apt update
sudo apt-get install openjdk-8-jre -y
cd /opt
sudo wget -O nexus.tar.gz https://download.sonatype.com/nexus/3/latest-unix.tar.gz
sudo tar -xvf nexus.tar.gz
sudo mv nexus-3* nexus
sudo adduser nexus
sudo chown -R nexus:nexus /opt/nexus
sudo chown -R nexus:nexus /opt/sonatype-work
#sed -i 's @(#run_as_user="") @(run_as_user="nexus") ' /opt/nexus/bin/nexus.rc
sudo sed -i 'a run_as_user="nexus" ' /opt/nexus/bin/nexus.rc
su -nexus
/opt/nexus/bin/nexus start
EOF
}
  