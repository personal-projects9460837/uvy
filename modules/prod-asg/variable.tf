variable "priv_subnet1" {}
variable "priv_subnet2" {}
variable "prod_target_gp_arn" {}
variable "instance_type" {}
variable "ami_ubuntu" {}
variable "key_pair" {}
variable "docker_prod_sg" {}