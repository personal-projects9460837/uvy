locals {
  sonarqube_user_data = <<-EOF
#!/bin/bash
sudo apt update
sudo hostnamectl set-hostname sonarqube
sudo apt install docker.io -y
sudo systemctl start docker
sudo systemctl enable docker
sudo usermod -aG docker ubuntu
docker run -it -d --name sonarqube -p 9000:9000 sonarqube
EOF
}
