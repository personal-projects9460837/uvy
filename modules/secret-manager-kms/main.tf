#Generate KMS key to use to encrypt key in secret manager
resource "aws_kms_key" "kms_rds" {
  description             = "kms key for rds"
  deletion_window_in_days = 10
  is_enabled              = true
  enable_key_rotation     = true  

  tags = {
    Name = "kms_rds_key"
  }
}

resource "random_password" "rds_password" {
  length           = 16
}

resource "aws_secretsmanager_secret" "rds" {
  name                    = "rds_admin_secret"
  description             = "secret for rds admin"
  kms_key_id              = aws_kms_key.kms_rds.id
  recovery_window_in_days = 30

  tags = {
    Name = "rds_secret"
  }
}

resource "aws_secretsmanager_secret_version" "name" {
  secret_id = aws_secretsmanager_secret.rds.id
  secret_string = random_password.rds_password.result
}