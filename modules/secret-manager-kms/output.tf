output "kms_id" {
  value = aws_kms_key.kms_rds.id
}

output "secret_id" {
  value = aws_secretsmanager_secret.rds.id
}

output "random_pass" {
  value = random_password.rds_password.result
}
