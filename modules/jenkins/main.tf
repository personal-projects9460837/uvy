resource "aws_instance" "Jenkins_Server" {
  ami                           = var.ami_ubuntu
  instance_type                 = "t2.medium"
  key_name                      = var.key_pair
  subnet_id                     = var.priv_subnet1
  vpc_security_group_ids        = [ var.jenkins_sg ]
  user_data              = templatefile("./modules/jenkins/jenkins.sh", {
    nexus_pub_ip = var.nexus_pub_ip
  })

  tags = {
    Name = "Jenkins_Server"
  }
}