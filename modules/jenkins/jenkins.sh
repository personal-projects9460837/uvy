#!/bin/bash
sudo hostnamectl set-hostname jenkins
sudo apt update
sudo apt install git -y
sudo apt install openjdk-11-jdk -y
curl -fsSL https://pkg.jenkins.io/debian-stable/jenkins.io-2023.key | sudo tee \
    /usr/share/keyrings/jenkins-keyring.asc > /dev/null
echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] \
    https://pkg.jenkins.io/debian-stable binary/ | sudo tee \
    /etc/apt/sources.list.d/jenkins.list > /dev/null
sudo apt-get update
sudo apt-get install jenkins -y
sudo systemctl start jenkins
sudo apt install docker.io -y
sudo systemctl start docker
sudo systemctl enable docker
sudo usermod -aG docker ubuntu
sudo chmod 777 /var/run/docker.sock
touch /etc/docker/daemon.json
cat > /etc/docker/daemon.json << EOT
{
  "insecure-registries" : ["${nexus_pub_ip}:8082"]
}
EOT

sudo systemctl restart docker