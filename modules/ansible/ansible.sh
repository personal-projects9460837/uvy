#!/bin/bash
sudo hostnamectl set-hostname ansible
sudo apt update
sudo apt-add-repository ppa:ansible/ansible
sudo apt update -y
sudo apt install ansible -y
sudo apt install python3-pip -y
sudo apt install python3-boto3 -y

# Copying private key into ansible Server
echo "${priv_key}" >> /home/ubuntu/.ssh/myKey
sudo chown -R ubuntu:ubuntu /home/ubuntu/.ssh/myKey
sudo chmod 400 /home/ubuntu/.ssh/myKey

#Creating ansible variables files
echo nexus_ip: "${nexus_pub_ip}:8082" > /home/ubuntu/projvars.yaml

#copying playbook files to ansible server
#echo "${file("./ansible/playbooks/prod.yml")}" >> /home/ubuntu/prod.yml
#echo "${file("./ansible/playbooks/stage.yml")}" >> /home/ubuntu/stage.yml

#Copying file containing nexus password to ansible server
echo "${file("./ansible/nexusPass.yaml")}" >> /home/ubuntu/nexusPass.yaml

#Creating a file to save ansible vault password for encrypting and decrypting.
touch /home/ubuntu/vaultpass.txt
sudo chown -R ubuntu:ubuntu /home/ubuntu/vaultpass.txt
sudo chmod 600 /home/ubuntu/vaultpass.txt
echo john >> /home/ubuntu/vaultpass.txt

#Encrypting the file that contain nexus password and using the file that contain the ansible vault password for encryption
ansible-vault encrypt /home/ubuntu/nexusPass.yaml --vault-password-file /home/ubuntu/vaultpass.txt

#Changing the ownership of the encrypted file to ubuntu
sudo chown -R ubuntu:ubuntu /home/ubuntu/nexusPass.yaml



