# launch ansible-server
resource "aws_instance" "ansible-server" {
  ami                    = var.ami_ubuntu
  instance_type          = "t2.micro"
  subnet_id              = var.priv_subnet1
  iam_instance_profile   = var.instance_profile
  vpc_security_group_ids = [var.ansible_sg]
  key_name               = var.key_pair 
  user_data              = templatefile("./modules/ansible/ansible.sh", {
    priv_key      = var.priv_key
    nexus_pub_ip  = var.nexus_pub_ip
    pub_key       = var.pub_key
  })

  tags = {
    Name = "ansible-webserver"
  }

  connection {
    type = "ssh"
    host = self.private_ip
    user = "ubuntu"
    private_key = var.priv_key
    bastion_host = var.bastion-host
    bastion_user = "ubuntu"
    bastion_host_key = var.priv_key

  }

  provisioner "file" {
    source = "./ansible.cfg"
    destination = "/home/ubuntu/ansible.cfg"
  }

  provisioner "file" {
    source = "./inventory_aws_ec2.yaml"
    destination = "/home/ubuntu/inventory_aws_ec2.yaml"
  }

  provisioner "file" {
    source = "./modules/ansible/playbooks"
    destination = "/home/ubuntu/playbooks"
  }

}