# #create db subnet group
resource "aws_db_subnet_group" "db_subnet_group" {
  name       = "db_subnet_group"
  subnet_ids = [var.priv_subnet1, var.priv_subnet1]
}

# #create RDS(database)
resource "aws_db_instance" "rds" {
  allocated_storage           = 10
  db_subnet_group_name        = aws_db_subnet_group.db_subnet_group.id
  engine                      = "mysql"
  engine_version              = "8.0"
  identifier                  = "mysql-rds"
  instance_class              = "db.t2.micro"
  multi_az                    = false
  db_name                     = var.dbName
  password                    = var.dbPassword
  username                    = var.dbUsername
  storage_type                = "gp2"
  vpc_security_group_ids      = [var.rds_sg]
  skip_final_snapshot         = true
  parameter_group_name        = "default.mysql8.0"
  storage_encrypted           = true 
}