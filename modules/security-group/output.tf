output "lb_sg" {
  value = aws_security_group.lb_sg.id
}
output "bastion_sg" {
  value = aws_security_group.bastion-sg.id
}

output "ansible_sg" {
  value = aws_security_group.ansible-sg.id
}

output "jenkins_sg" {
  value = aws_security_group.jenkins-sg.id 
}

output "sonar_sg" {
  value = aws_security_group.sonarqube_security_group.id
}

output "nexus_sg" {
  value = aws_security_group.nexus_security_group.id
}

output "docker_prod_sg" {
  value = aws_security_group.docker_prod_sg.id
}

output "docker_stage_sg" {
  value = aws_security_group.docker_stage_sg.id
}

output "rds_sg" {
  value = aws_security_group.rds_sg.id
}