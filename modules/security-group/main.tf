resource "aws_security_group" "lb_sg" {
  name = "loadbalancer security group"
  description = "Allow inbound traffic from port 80 and 443"
  vpc_id = var.vpc_id

  ingress {
    description      = "http access"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = [var.sg-cidr]
  }

  ingress {
    description      = "https access"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = [var.sg-cidr]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = [var.sg-cidr]
  }
  
}

resource "aws_security_group" "bastion-sg" {
  name        = "bastion security group"
  description = "Allow inbound traffic from port 22"
  vpc_id      = var.vpc_id

  ingress {
    description      = "ssh access"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = [var.sg-cidr]
  } 

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = [var.sg-cidr]
  }

  tags = {
    Name = "bastion-sg"
    }
}

resource "aws_security_group" "ansible-sg" {
  name        = "ansible security group"
  description = "Allow inbound traffic from port 22"
  vpc_id      = var.vpc_id
  ingress {
    description      = "ssh access"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    security_groups  = [aws_security_group.bastion-sg.id ] 
    cidr_blocks      = [ "${var.jenkins-priv-ip}/32" ]
    
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = [var.sg-cidr]
  }

  tags = {
    Name = "ansible-sg"
  }
}

resource "aws_security_group" "jenkins-sg" {
  name        = "jenkins security group"
  description = "Allow inbound traffic from port 22, 8080, 80 and 443"
  vpc_id      = var.vpc_id

  ingress {
    description         = "ssh access"
    from_port           = 22
    to_port             = 22
    protocol            = "tcp"
    security_groups     = [aws_security_group.bastion-sg.id]
  }

  ingress {
    description      = "jenkins port access"
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = [var.sg-cidr]
  }

  ingress {
    description         = "http access"
    from_port           = 80
    to_port             = 80
    protocol            = "tcp"
    security_groups     = [aws_security_group.lb_sg.id]
  }

  ingress {
    description         = "https access"
    from_port           = 443
    to_port             = 443
    protocol            = "tcp"
    security_groups     = [aws_security_group.lb_sg.id]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = [var.sg-cidr]
  }

  tags = {
    Name = "jenkins-sg"
  }
}

# create security group for the ec2 inst
resource "aws_security_group" "sonarqube_security_group" {
  name        = "sonarqube security group"
  description = "allow access on ports 22 and 9000"
  vpc_id      = var.vpc_id

  # allow access on port 22
  ingress {
    description         = "ssh access"
    from_port           = 22
    to_port             = 22
    protocol            = "tcp"
    cidr_blocks         = ["0.0.0.0/0"]
  }

  ingress {
    description = "sonarqube access"
    from_port   = 9000
    to_port     = 9000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "sonarqube-server-SG"
  }
}

# create security group for the ec2 instance
resource "aws_security_group" "nexus_security_group" {
  name        = "nexus sever security group"
  description = "allow access on ports 22, 8081 and 8082"
  vpc_id      = var.vpc_id

  # allow access on port 22, 8081 and 8082
  ingress {
    description = "ssh access"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "nexus server access"
    from_port   = 8081
    to_port     = 8081
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "nexus docker access"
    from_port   = 8082
    to_port     = 8082
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "nexus_server_security_group"
  }
}

# create security group for docker prod
resource "aws_security_group" "docker_prod_sg" {
  name                               = "docker prod security group"
  description                        = "allow access on ports 22 and port 3000"
  vpc_id                             = var.vpc_id

  # allow access on port 22
  ingress {
    description                      = "ssh access"
    from_port                        = 22
    to_port                          = 22
    protocol                         = "tcp"
    security_groups                  = [aws_security_group.bastion-sg.id]
    cidr_blocks                      = [ "${var.ansible-priv-ip}/32" ]
  }

  # allow access on port 3000
  ingress {
    description                      = "prod-access"
    from_port                        = 3000
    to_port                          = 3000
    protocol                         = "tcp"
    cidr_blocks                      = ["0.0.0.0/0"]
  }

  egress {
    from_port                       = 0
    to_port                         = 0
    protocol                        = "-1"
    cidr_blocks                     = ["0.0.0.0/0"]
  }

  tags = {
    Name                            = "docker_prod_security_group"
  }
}

# Creating security group for docker stage
resource "aws_security_group" "docker_stage_sg" {
  name                              = "docker stage security group"
  description                       = "allow access on ports 22 and port 4000"
  vpc_id                            = var.vpc_id

  # allow access on port 22
  ingress {
    description                      = "ssh access"
    from_port                        = 22
    to_port                          = 22
    protocol                         = "tcp"
    security_groups                  = [aws_security_group.bastion-sg.id]
    cidr_blocks                      = [ "${var.ansible-priv-ip}/32" ]
  }
  
  # allow access on port 4000
  ingress {
    description                      = "stage-access"
    from_port                        = 4000
    to_port                          = 4000
    protocol                         = "tcp"
    cidr_blocks                      = ["0.0.0.0/0"]
  }

  egress {
    from_port                        = 0
    to_port                          = 0
    protocol                         = "-1"
    cidr_blocks                      = ["0.0.0.0/0"]
  }

  tags = {
    Name                             = "docker_stage_sg"
  }
}

# Security Group for MySQL RDS Database
resource "aws_security_group" "rds_sg" {
  name        = "MySQL_RDS"
  description = "Allow inbound traffic"
  vpc_id      = var.vpc_id
  ingress {
    description      = "Allow MySQL access"
    from_port        = 3306
    to_port          = 3306
    protocol         = "tcp"
    security_groups = [ aws_security_group.docker_prod_sg.id, aws_security_group.docker_stage_sg.id ]
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "MySQL_RDS"
  }
}
