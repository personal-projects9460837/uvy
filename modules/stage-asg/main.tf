resource "aws_autoscaling_group" "stage_asg" {
  name                      = "docker_stage_asg"
  desired_capacity          = 1
  min_size                  = 1
  max_size                  = 2
  health_check_type         = "EC2" 
  health_check_grace_period = 120
  force_delete              = true
  vpc_zone_identifier       = [var.priv_subnet1, var.priv_subnet2]
  target_group_arns         = [var.stage_target_gp_arn]

  launch_template {
    id      = aws_launch_template.stage_template.id
    version = "$Latest"
  }
  
  tag {
    key                     = "Name"
    value                   = "docker_stage_asg"
    propagate_at_launch     = true
  }
  
}

resource "aws_launch_template" "stage_template" {
  name                   = "docker_stage_lt"
  instance_type          = var.instance_type
  image_id               = var.ami_ubuntu
  key_name               = var.key_pair
  vpc_security_group_ids = [var.docker_stage_sg]

  monitoring {
    enabled = true
  }

  tag_specifications {
    resource_type = "instance"
    tags = {
      Name   = "docker_stage_lt"
      Source = "Autoscaling"
    }
  }

}

# Creating ASG Policy
resource "aws_autoscaling_policy" "Team1-ASG-Policy" {
  name                      = "Docker_stage_ASG_Policy2"
  autoscaling_group_name    = aws_autoscaling_group.stage_asg.name
  policy_type               = "TargetTrackingScaling"
  estimated_instance_warmup = 300

  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }
    target_value = 60.0
  }
}